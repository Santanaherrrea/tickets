﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tickets.Models
{
    public class DetalleFactura
    {
        [Key]
        public int idDetalleFactura { get; set; }
        [Required]
        public int fecha { get; set; }
        [Required]
        public int total { get; set; }

        public int idFactura { get; set; }
        public Factura factura { get; set; }
    }
}