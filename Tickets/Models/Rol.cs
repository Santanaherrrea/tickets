﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tickets.Models
{
    public class Rol
    {
        [Key]
        public int idRol { get; set; }
        [Required]
        public string rol { get; set; }
    }
}