﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Tickets.Models
{
    public class TicketsContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public TicketsContext() : base("name=TicketsContext")
        {
        }

        public System.Data.Entity.DbSet<Tickets.Models.Boleto> Boletoes { get; set; }

        public System.Data.Entity.DbSet<Tickets.Models.Evento> Eventoes { get; set; }

        public System.Data.Entity.DbSet<Tickets.Models.DetalleFactura> DetalleFacturas { get; set; }

        public System.Data.Entity.DbSet<Tickets.Models.Factura> Facturas { get; set; }

        public System.Data.Entity.DbSet<Tickets.Models.Usuario> Usuarios { get; set; }

        public System.Data.Entity.DbSet<Tickets.Models.Rol> Rols { get; set; }

        public System.Data.Entity.DbSet<Tickets.Models.TipoEventos> TipoEventos { get; set; }
    }
}
