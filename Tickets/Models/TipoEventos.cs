﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tickets.Models
{
    public class TipoEventos
    {
        [Key]
        public int idTipoEventos { get; set; }
        [Required]
        public string tipoEventos { get; set; }
    }
}