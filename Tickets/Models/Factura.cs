﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Tickets.Models
{
    public class Factura
    {
        [Key]
        public int idFactura { get; set; }

        public int idUsuario { get; set; }
        public Usuario usuario { get; set; }
        
        public int idBoleto { get; set; }
        public Boleto boleto { get; set; }
    }
}