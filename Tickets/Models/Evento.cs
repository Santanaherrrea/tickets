﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Tickets.Models
{
    public class Evento
    {
        [Key]
        public int idEvento { get; set; }
        [Required]
        public string descripcion { get; set; }
        [Required]
        public string lugarEvento { get; set; }
        [Required]
        public string cupoEvento { get; set; }
        [Required]
        public string fechaHoraEvento { get; set; }

        public int idTipoEventos { get; set; }
        public Rol tipoEventos { get; set; }

    }
}