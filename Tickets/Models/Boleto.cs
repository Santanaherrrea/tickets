﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Tickets.Models
{
    public class Boleto
    {
        [Key]
        public int idBoleto { get; set; }
        [Required]
        public int precio { get; set; }

        public int idEvento { get; set; }
        public Evento evento { get; set; }
    }
}