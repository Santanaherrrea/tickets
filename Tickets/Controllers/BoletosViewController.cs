﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tickets.Models;

namespace Tickets.Controllers
{
    public class BoletosViewController : Controller
    {
        private TicketsContext db = new TicketsContext();

        // GET: BoletosView
        public async Task<ActionResult> Index()
        {
            var boletoes = db.Boletoes.Include(b => b.evento);
            return View(await boletoes.ToListAsync());
        }

        // GET: BoletosView/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Boleto boleto = await db.Boletoes.FindAsync(id);
            if (boleto == null)
            {
                return HttpNotFound();
            }
            return View(boleto);
        }

        // GET: BoletosView/Create
        public ActionResult Create()
        {
            ViewBag.idEvento = new SelectList(db.Eventoes, "idEvento", "descripcion");
            return View();
        }

        // POST: BoletosView/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "idBoleto,precio,idEvento")] Boleto boleto)
        {
            if (ModelState.IsValid)
            {
                db.Boletoes.Add(boleto);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.idEvento = new SelectList(db.Eventoes, "idEvento", "descripcion", boleto.idEvento);
            return View(boleto);
        }

        // GET: BoletosView/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Boleto boleto = await db.Boletoes.FindAsync(id);
            if (boleto == null)
            {
                return HttpNotFound();
            }
            ViewBag.idEvento = new SelectList(db.Eventoes, "idEvento", "descripcion", boleto.idEvento);
            return View(boleto);
        }

        // POST: BoletosView/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "idBoleto,precio,idEvento")] Boleto boleto)
        {
            if (ModelState.IsValid)
            {
                db.Entry(boleto).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.idEvento = new SelectList(db.Eventoes, "idEvento", "descripcion", boleto.idEvento);
            return View(boleto);
        }

        // GET: BoletosView/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Boleto boleto = await db.Boletoes.FindAsync(id);
            if (boleto == null)
            {
                return HttpNotFound();
            }
            return View(boleto);
        }

        // POST: BoletosView/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Boleto boleto = await db.Boletoes.FindAsync(id);
            db.Boletoes.Remove(boleto);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
