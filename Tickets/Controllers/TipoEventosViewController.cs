﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tickets.Models;

namespace Tickets.Controllers
{
    public class TipoEventosViewController : Controller
    {
        private TicketsContext db = new TicketsContext();

        // GET: TipoEventosView
        public async Task<ActionResult> Index()
        {
            return View(await db.TipoEventos.ToListAsync());
        }

        // GET: TipoEventosView/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoEventos tipoEventos = await db.TipoEventos.FindAsync(id);
            if (tipoEventos == null)
            {
                return HttpNotFound();
            }
            return View(tipoEventos);
        }

        // GET: TipoEventosView/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: TipoEventosView/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "idTipoEventos,tipoEventos")] TipoEventos tipoEventos)
        {
            if (ModelState.IsValid)
            {
                db.TipoEventos.Add(tipoEventos);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(tipoEventos);
        }

        // GET: TipoEventosView/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoEventos tipoEventos = await db.TipoEventos.FindAsync(id);
            if (tipoEventos == null)
            {
                return HttpNotFound();
            }
            return View(tipoEventos);
        }

        // POST: TipoEventosView/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "idTipoEventos,tipoEventos")] TipoEventos tipoEventos)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tipoEventos).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(tipoEventos);
        }

        // GET: TipoEventosView/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TipoEventos tipoEventos = await db.TipoEventos.FindAsync(id);
            if (tipoEventos == null)
            {
                return HttpNotFound();
            }
            return View(tipoEventos);
        }

        // POST: TipoEventosView/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            TipoEventos tipoEventos = await db.TipoEventos.FindAsync(id);
            db.TipoEventos.Remove(tipoEventos);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
