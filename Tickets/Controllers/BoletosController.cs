﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Tickets.Models;

namespace Tickets.Controllers
{
    public class BoletosController : ApiController
    {
        private TicketsContext db = new TicketsContext();

        // GET: api/Boletos
        public IQueryable<Boleto> GetBoletoes()
        {
            return db.Boletoes;
        }

        // GET: api/Boletos/5
        [ResponseType(typeof(Boleto))]
        public async Task<IHttpActionResult> GetBoleto(int id)
        {
            Boleto boleto = await db.Boletoes.FindAsync(id);
            if (boleto == null)
            {
                return NotFound();
            }

            return Ok(boleto);
        }

        // PUT: api/Boletos/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBoleto(int id, Boleto boleto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != boleto.idBoleto)
            {
                return BadRequest();
            }

            db.Entry(boleto).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BoletoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Boletos
        [ResponseType(typeof(Boleto))]
        public async Task<IHttpActionResult> PostBoleto(Boleto boleto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Boletoes.Add(boleto);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = boleto.idBoleto }, boleto);
        }

        // DELETE: api/Boletos/5
        [ResponseType(typeof(Boleto))]
        public async Task<IHttpActionResult> DeleteBoleto(int id)
        {
            Boleto boleto = await db.Boletoes.FindAsync(id);
            if (boleto == null)
            {
                return NotFound();
            }

            db.Boletoes.Remove(boleto);
            await db.SaveChangesAsync();

            return Ok(boleto);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BoletoExists(int id)
        {
            return db.Boletoes.Count(e => e.idBoleto == id) > 0;
        }
    }
}