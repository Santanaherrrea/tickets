﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tickets.Models;

namespace Tickets.Controllers
{
    public class FacturasViewController : Controller
    {
        private TicketsContext db = new TicketsContext();

        // GET: FacturasView
        public async Task<ActionResult> Index()
        {
            var facturas = db.Facturas.Include(f => f.boleto).Include(f => f.usuario);
            return View(await facturas.ToListAsync());
        }

        // GET: FacturasView/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Factura factura = await db.Facturas.FindAsync(id);
            if (factura == null)
            {
                return HttpNotFound();
            }
            return View(factura);
        }

        // GET: FacturasView/Create
        public ActionResult Create()
        {
            ViewBag.idBoleto = new SelectList(db.Boletoes, "idBoleto", "idBoleto");
            ViewBag.idUsuario = new SelectList(db.Usuarios, "idUsuario", "nombre");
            return View();
        }

        // POST: FacturasView/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "idFactura,idUsuario,idBoleto")] Factura factura)
        {
            if (ModelState.IsValid)
            {
                db.Facturas.Add(factura);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.idBoleto = new SelectList(db.Boletoes, "idBoleto", "idBoleto", factura.idBoleto);
            ViewBag.idUsuario = new SelectList(db.Usuarios, "idUsuario", "nombre", factura.idUsuario);
            return View(factura);
        }

        // GET: FacturasView/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Factura factura = await db.Facturas.FindAsync(id);
            if (factura == null)
            {
                return HttpNotFound();
            }
            ViewBag.idBoleto = new SelectList(db.Boletoes, "idBoleto", "idBoleto", factura.idBoleto);
            ViewBag.idUsuario = new SelectList(db.Usuarios, "idUsuario", "nombre", factura.idUsuario);
            return View(factura);
        }

        // POST: FacturasView/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "idFactura,idUsuario,idBoleto")] Factura factura)
        {
            if (ModelState.IsValid)
            {
                db.Entry(factura).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.idBoleto = new SelectList(db.Boletoes, "idBoleto", "idBoleto", factura.idBoleto);
            ViewBag.idUsuario = new SelectList(db.Usuarios, "idUsuario", "nombre", factura.idUsuario);
            return View(factura);
        }

        // GET: FacturasView/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Factura factura = await db.Facturas.FindAsync(id);
            if (factura == null)
            {
                return HttpNotFound();
            }
            return View(factura);
        }

        // POST: FacturasView/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Factura factura = await db.Facturas.FindAsync(id);
            db.Facturas.Remove(factura);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
