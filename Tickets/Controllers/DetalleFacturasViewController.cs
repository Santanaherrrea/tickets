﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Tickets.Models;

namespace Tickets.Controllers
{
    public class DetalleFacturasViewController : Controller
    {
        private TicketsContext db = new TicketsContext();

        // GET: DetalleFacturasView
        public async Task<ActionResult> Index()
        {
            var detalleFacturas = db.DetalleFacturas.Include(d => d.factura);
            return View(await detalleFacturas.ToListAsync());
        }

        // GET: DetalleFacturasView/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetalleFactura detalleFactura = await db.DetalleFacturas.FindAsync(id);
            if (detalleFactura == null)
            {
                return HttpNotFound();
            }
            return View(detalleFactura);
        }

        // GET: DetalleFacturasView/Create
        public ActionResult Create()
        {
            ViewBag.idFactura = new SelectList(db.Facturas, "idFactura", "idFactura");
            return View();
        }

        // POST: DetalleFacturasView/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "idDetalleFactura,fecha,total,idFactura")] DetalleFactura detalleFactura)
        {
            if (ModelState.IsValid)
            {
                db.DetalleFacturas.Add(detalleFactura);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            ViewBag.idFactura = new SelectList(db.Facturas, "idFactura", "idFactura", detalleFactura.idFactura);
            return View(detalleFactura);
        }

        // GET: DetalleFacturasView/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetalleFactura detalleFactura = await db.DetalleFacturas.FindAsync(id);
            if (detalleFactura == null)
            {
                return HttpNotFound();
            }
            ViewBag.idFactura = new SelectList(db.Facturas, "idFactura", "idFactura", detalleFactura.idFactura);
            return View(detalleFactura);
        }

        // POST: DetalleFacturasView/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "idDetalleFactura,fecha,total,idFactura")] DetalleFactura detalleFactura)
        {
            if (ModelState.IsValid)
            {
                db.Entry(detalleFactura).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            ViewBag.idFactura = new SelectList(db.Facturas, "idFactura", "idFactura", detalleFactura.idFactura);
            return View(detalleFactura);
        }

        // GET: DetalleFacturasView/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DetalleFactura detalleFactura = await db.DetalleFacturas.FindAsync(id);
            if (detalleFactura == null)
            {
                return HttpNotFound();
            }
            return View(detalleFactura);
        }

        // POST: DetalleFacturasView/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            DetalleFactura detalleFactura = await db.DetalleFacturas.FindAsync(id);
            db.DetalleFacturas.Remove(detalleFactura);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
