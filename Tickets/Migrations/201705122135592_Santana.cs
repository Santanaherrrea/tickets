namespace Tickets.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Santana : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Boletoes",
                c => new
                    {
                        idBoleto = c.Int(nullable: false, identity: true),
                        precio = c.Int(nullable: false),
                        idEvento = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idBoleto)
                .ForeignKey("dbo.Eventoes", t => t.idEvento, cascadeDelete: true)
                .Index(t => t.idEvento);
            
            CreateTable(
                "dbo.Eventoes",
                c => new
                    {
                        idEvento = c.Int(nullable: false, identity: true),
                        descripcion = c.String(nullable: false),
                        lugarEvento = c.String(nullable: false),
                        cupoEvento = c.String(nullable: false),
                        fechaHoraEvento = c.String(nullable: false),
                        idTipoEventos = c.Int(nullable: false),
                        tipoEventos_idRol = c.Int(),
                    })
                .PrimaryKey(t => t.idEvento)
                .ForeignKey("dbo.Rols", t => t.tipoEventos_idRol)
                .Index(t => t.tipoEventos_idRol);
            
            CreateTable(
                "dbo.Rols",
                c => new
                    {
                        idRol = c.Int(nullable: false, identity: true),
                        rol = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.idRol);
            
            CreateTable(
                "dbo.DetalleFacturas",
                c => new
                    {
                        idDetalleFactura = c.Int(nullable: false, identity: true),
                        fecha = c.Int(nullable: false),
                        total = c.Int(nullable: false),
                        idFactura = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idDetalleFactura)
                .ForeignKey("dbo.Facturas", t => t.idFactura, cascadeDelete: true)
                .Index(t => t.idFactura);
            
            CreateTable(
                "dbo.Facturas",
                c => new
                    {
                        idFactura = c.Int(nullable: false, identity: true),
                        idUsuario = c.Int(nullable: false),
                        idBoleto = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idFactura)
                .ForeignKey("dbo.Boletoes", t => t.idBoleto, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.idUsuario, cascadeDelete: true)
                .Index(t => t.idUsuario)
                .Index(t => t.idBoleto);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        idUsuario = c.Int(nullable: false, identity: true),
                        nombre = c.String(nullable: false),
                        apellido = c.String(nullable: false),
                        dpi = c.Int(nullable: false),
                        tarjeta = c.String(nullable: false),
                        correo = c.String(nullable: false),
                        direccion = c.String(nullable: false),
                        contrasena = c.String(nullable: false),
                        idRol = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.idUsuario)
                .ForeignKey("dbo.Rols", t => t.idRol, cascadeDelete: true)
                .Index(t => t.idRol);
            
            CreateTable(
                "dbo.TipoEventos",
                c => new
                    {
                        idTipoEventos = c.Int(nullable: false, identity: true),
                        tipoEventos = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.idTipoEventos);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DetalleFacturas", "idFactura", "dbo.Facturas");
            DropForeignKey("dbo.Facturas", "idUsuario", "dbo.Usuarios");
            DropForeignKey("dbo.Usuarios", "idRol", "dbo.Rols");
            DropForeignKey("dbo.Facturas", "idBoleto", "dbo.Boletoes");
            DropForeignKey("dbo.Boletoes", "idEvento", "dbo.Eventoes");
            DropForeignKey("dbo.Eventoes", "tipoEventos_idRol", "dbo.Rols");
            DropIndex("dbo.Usuarios", new[] { "idRol" });
            DropIndex("dbo.Facturas", new[] { "idBoleto" });
            DropIndex("dbo.Facturas", new[] { "idUsuario" });
            DropIndex("dbo.DetalleFacturas", new[] { "idFactura" });
            DropIndex("dbo.Eventoes", new[] { "tipoEventos_idRol" });
            DropIndex("dbo.Boletoes", new[] { "idEvento" });
            DropTable("dbo.TipoEventos");
            DropTable("dbo.Usuarios");
            DropTable("dbo.Facturas");
            DropTable("dbo.DetalleFacturas");
            DropTable("dbo.Rols");
            DropTable("dbo.Eventoes");
            DropTable("dbo.Boletoes");
        }
    }
}
